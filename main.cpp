#include <cmath>
#include <ctime>
#include <iostream>
#include "resource.h"
#include <vector>
#include <windows.h>
#include <commctrl.h>
using namespace std;

const char g_szClassName[] = "myWindowClass";
HWND button;
HWND label;
HWND track;

const int size = 513;
bool showTools = true;

double frand(double fmin, double fmax) {
    double f = (double)rand() / RAND_MAX;
    return fmin + f * (fmax - fmin);
}

class DSLandScape {
    private:
    double grid[size][size];
	int max = log2(size - 1);

    double roughness = 0.6;
    int sealevel = 125;
    int sandbarrier = 5;

    void diamondStep(int i) {
        vector<int> u;
        vector<int> v;

        for(int n = 0; n < exp2(i + 1) + 1; n++) {
            int x = (n * exp2(max - i)) / 2;
            if(n % 2 == 0) {
                u.push_back(x);
            }
            else {
                v.push_back(x);
            }
        }

        for(int a = 0; a < u.size(); a++) {
            for(int b = 0; b < v.size(); b++) {
                grid[u[a]][v[b]] = fetchDiamond(i + 1, u[a], v[b]);
            }
        }
        for(int a = 0; a < v.size(); a++) {
            for(int b = 0; b < u.size(); b++) {
                grid[v[a]][u[b]] = fetchDiamond(i + 1, v[a], u[b]);
            }
        }
    }

    double fetchDiamond(int i, int x, int y) {
        int side =  exp2(max - i);
        double avg = (grid[x][wrap(y + side)] + grid[x][wrap(y - side)] + grid[wrap(x - side)][y] + grid[wrap(x + side)][y]) / 4.0;
        double delta = frand(-pow(roughness, i), pow(roughness, i));

        return avg + delta;
    }

    double fetchSquare(int i, int x, int y) {
        int side =  exp2(max - i);
        double avg = (grid[x + side][y + side] + grid[x - side][y - side] + grid[x - side][y + side] + grid[x + side][y - side]) / 4.0;
        double delta = frand(-pow(roughness, i), pow(roughness, i));

        return avg + delta;
    }

    double findLargest() {
        double largest = 0;

        for(int x = 0; x < size; x++) {
            for(int y = 0; y < size; y++) {
                if(grid[x][y] > largest) {
                    largest = grid[x][y];
                }
            }
        }
        return largest;
    }

    double findSmallest() {
        double smallest = 0;

        for(int x = 0; x < size; x++) {
            for(int y = 0; y < size; y++) {
                if(grid[x][y] < smallest) {
                    smallest = grid[x][y];
                }
            }
        }
        return smallest;
    }

    void squareStep(int i) {
        vector<int> v;

        for(int n = 0; n < exp2(i - 1); n++) {
            int x = n * exp2(max - i + 1) + (size - 1) / exp2(i);
            v.push_back(x);
        }

        for(int a = 0; a < v.size(); a++) {
            for(int b = 0; b < v.size(); b++) {
                grid[v[a]][v[b]] = fetchSquare(i, v[a], v[b]);
            }
        }
    }

    int wrap(int coord) {
        if(coord > size - 1) {
            return coord - size + 1;
        }
        else if(coord < 0) {
            return coord + size - 1;
        }
        else {
            return coord;
        }
    }

    public:
    void calculate() {
        for(int i = 0; i < max; i++) {
            squareStep(i + 1);
            diamondStep(i);
        }
    }

    void init() {
        for(int x = 0; x < size; x++) {
            for(int y = 0; y < size; y++) {
                grid[x][y] = 0.0;
            }
        }

        srand(time(NULL));
        grid[0][0] = frand(-1, 1);
        grid[0][size - 1] = frand(-1, 1);
        grid[size - 1][0] = frand(-1, 1);
        grid[size - 1][size - 1] = frand(-1, 1);
    }

    void render(HDC hdc, RECT rc) {
        cout << rc.bottom << " " << rc.right << endl;
        double smallest = findSmallest();
        double largest = findLargest();
        double m = 255.0 / (largest - smallest);

        for(int y = rc.top; y < rc.bottom; y++) {
            for(int x = rc.left; x < rc.right; x++) {
                int v = (grid[x][y] - smallest) * m;

                if(v >= sealevel + sandbarrier) {
                    if(v - sealevel - sandbarrier + 109 > 216) {
                        SetPixel(hdc, x, y, RGB(20, 216, 10));
                    }
                    else {
                        SetPixel(hdc, x, y, RGB(20, v - sealevel - sandbarrier + 109, 10));
                    }
                }
                else if(v >= sealevel) {
                    SetPixel(hdc, x, y, RGB(191 + sealevel + sandbarrier - v, 180, 130));
                }
                else {
                    if(74 - sealevel + v < 0) {
                        SetPixel(hdc, x, y, RGB(0, 0, 120));
                    }
                    else {
                        SetPixel(hdc, x, y, RGB(0, 74 - sealevel + v, 120));
                    }
                }
            }
        }
    }

    void setSeaLevel(int l) {
        if(l >= -5 && l <= 255) {
            sealevel = l;
        }
    }

} obj;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
        case WM_LBUTTONDOWN:
        {
            if(showTools) {
                SetWindowLongPtr(button, GWL_STYLE, WS_CHILD|WS_TABSTOP|BS_PUSHBUTTON);
                SetWindowLongPtr(label, GWL_STYLE, WS_CHILD|SS_CENTERIMAGE);
                SetWindowLongPtr(track, GWL_STYLE, WS_CHILD|TBS_AUTOTICKS);
                showTools = false;
            }
            else {
                SetWindowLongPtr(button, GWL_STYLE, WS_VISIBLE|WS_CHILD|WS_TABSTOP|BS_PUSHBUTTON);
                SetWindowLongPtr(label, GWL_STYLE, WS_VISIBLE|WS_CHILD|SS_CENTERIMAGE);
                SetWindowLongPtr(track, GWL_STYLE, WS_VISIBLE|WS_CHILD|TBS_AUTOTICKS);
                showTools = true;
            }
            RECT rc = {5, size - 35, size - 5, size - 5};
            InvalidateRect(hwnd, &rc, FALSE);
        }
        case WM_HSCROLL:
        {
            DWORD dwPos;
            switch (LOWORD(wParam)) {
                case TB_ENDTRACK:
                dwPos = SendMessage(track, TBM_GETPOS, 0, 0);

                if (dwPos > 260) {
                    SendMessage(track, TBM_SETPOS, (WPARAM) TRUE, (LPARAM) 260);
                    obj.setSeaLevel(255);
                }
                else if (dwPos < 0) {
                    SendMessage(track, TBM_SETPOS, (WPARAM) TRUE, (LPARAM) 0);
                    obj.setSeaLevel(-5);
                }
                else {
                    obj.setSeaLevel(dwPos - 5);
                }
                InvalidateRect(hwnd, NULL, FALSE);
                break;
                default:
                break;
            }
        }
        case WM_COMMAND:
        switch(LOWORD(wParam)) {
            case IDI_BUTTON:
                obj.setSeaLevel(125);
                obj.calculate();
                SendMessage(track, TBM_SETPOS, (WPARAM) TRUE, (LPARAM) 130);
                InvalidateRect(hwnd, NULL, FALSE);
            break;
        }
        break;
        case WM_CREATE:
        {
            button = CreateWindow("BUTTON", "Regenerate",
            WS_VISIBLE|WS_CHILD|WS_TABSTOP|BS_PUSHBUTTON, size - 105,
            size - 35, 100, 30, hwnd, (HMENU)IDI_BUTTON,
            GetModuleHandle(NULL), NULL);

            label = CreateWindow("STATIC", "Sea Level", WS_VISIBLE|WS_CHILD|SS_CENTERIMAGE,
            11, size - 33, 65, 24, hwnd, (HMENU)IDI_LABEL, GetModuleHandle(NULL), NULL);

            InitCommonControls();

            track = CreateWindowEx(0, TRACKBAR_CLASS, "Sealevel",
            WS_VISIBLE|WS_CHILD|TBS_AUTOTICKS, 78,
            size - 33, size - 200, 24, hwnd, (HMENU)IDI_TRACKBAR,
            GetModuleHandle(NULL), NULL);

            SendMessage(track, TBM_SETRANGE, (WPARAM) TRUE, (LPARAM) MAKELONG(0, 255 + 5));
            SendMessage(track, TBM_SETPAGESIZE, 0, (LPARAM) 4);
            SendMessage(track, TBM_SETPOS, (WPARAM) TRUE, (LPARAM) 130);

            obj.init();
            obj.calculate();
        }
		break;
        case WM_CLOSE:
			DestroyWindow(hwnd);
		break;
        case WM_PAINT:
		{
			PAINTSTRUCT ps;

			HDC hdc = BeginPaint(hwnd, &ps);
			obj.render(hdc, ps.rcPaint);

			if(showTools) {
                RECT rc = {5, size - 35, size - 115, size -5};
                FillRect(hdc, &rc, (HBRUSH) COLOR_WINDOW);
                DrawEdge(hdc, &rc, EDGE_RAISED, BF_RECT|BF_SOFT);
			}

			EndPaint(hwnd, &ps);
		}
		break;
        case WM_DESTROY:
			PostQuitMessage(0);
		break;
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;

	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.style		 = 0;
	wc.lpfnWndProc	 = WndProc;
	wc.cbClsExtra	 = 0;
	wc.cbWndExtra	 = 0;
	wc.hInstance	 = hInstance;
	wc.hIcon		 = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON));
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm		 = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON));

	if(!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Window Registration Failed!", "Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	RECT rc = {0, 0, size, size};
	AdjustWindowRectEx(&rc, WS_OVERLAPPEDWINDOW, FALSE, WS_EX_CLIENTEDGE);

	hwnd = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		g_szClassName,
		"Fractal Landscape Generator",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top,
		NULL, NULL, hInstance, NULL);

	if(hwnd == NULL)
	{
		MessageBox(NULL, "Window Creation Failed!", "Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return Msg.wParam;
}
